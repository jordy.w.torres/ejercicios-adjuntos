----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:26:28 05/18/2022 
-- Design Name: 
-- Module Name:    Motor - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Motor is
    Port ( A : in  STD_LOGIC;
           B : in  STD_LOGIC;
           C : in  STD_LOGIC;
           Ab : in  STD_LOGIC;
           Cb : in  STD_LOGIC;
           y : out  STD_LOGIC);
end Motor;

architecture Behavioral of Motor is

begin
Ab <= '0' when a ='0' else
      '1' when (a='1' and b ='1');
Cb <= '0' when c = '1' and b = '1');
      '1' when (c='1' and b ='1');
y<= ab or cb (a and c);
end Behavioral;

