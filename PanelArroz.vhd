----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:38:55 05/17/2022 
-- Design Name: 
-- Module Name:    Silos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Empaquetamiento_Arroz is
    Port ( a , b : in  STD_LOGIC;
           luz_c, luz_d, alarma_e : out  STD_LOGIC);
end Empaquetamiento_Arroz;

architecture Behavioral of Empaquetamiento_Arroz is


begin
luz_c <= '1' when (a or b) = '0' else '0'; 
luz_d <= '1' when (a and b)= '1' else '0';
alarma_e <= '1' when (a and b)= '1' else '0';
end Behavioral;

