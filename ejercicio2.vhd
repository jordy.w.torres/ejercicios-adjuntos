library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
entity prueba is
Port ( A, B, C: in STD_LOGIC;
X, Y, Z : out STD_LOGIC);
end prueba;

architecture ejercicio of prueba is
begin
--Las ecuaciones estan simplificadas
X<=((NOT A) AND (NOT B)) OR (B AND C);
Y<= ((NOT B)AND C)OR(A AND B AND(NOT C));
Z<= ((NOT A)AND(NOT C))OR((NOT A)AND B);
end ejercicio;